import 'package:contacts/src/modules/contacts/bloc/contact_bloc.dart';
import 'package:contacts/src/modules/contacts/bloc/contact_edit_bloc.dart';
import 'package:contacts/src/modules/contacts/bloc/contact_save_bloc.dart';
import 'package:contacts/src/modules/contacts/bloc/contacts_bloc.dart';
import 'package:contacts/src/modules/contacts/models/contact.dart';
import 'package:contacts/src/modules/contacts/repositories/contact_repository.dart';
import 'package:contacts/src/modules/contacts/views/contact_create_view.dart';
import 'package:contacts/src/modules/contacts/views/contact_details_view.dart';
import 'package:contacts/src/modules/contacts/views/contact_edit_view.dart';
import 'package:contacts/src/modules/contacts/views/contact_index_view.dart';
import 'package:contacts/src/shared/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  initializeServices();
  await services.allReady();
  runApp(ContactsApp());
}

class ContactsApp extends StatelessWidget {
  ContactsApp({super.key});

  final _router = GoRouter(routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const ContactIndexView(),
    ),
    GoRoute(
      path: '/create',
      builder: (context, state) => const ContactCreateView(),
    ),
    GoRoute(
      path: '/details/:id',
      builder: (context, state) {
        return ContactDetailsView(id: int.parse(state.params['id']!));
      },
    ),
    GoRoute(
      path: '/details/:id/edit',
      builder: (context, state) {
        return BlocProvider(
          create: (context) => ContactEditBloc(
            repository: services.get<ContactRepository>(),
            contact: state.extra as Contact,
          ),
          child: ContactEditView(
            id: int.parse(state.params['id']!),
          ),
        );
      },
    )
  ]);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => ContactsBloc(
            repository: services.get<ContactRepository>(),
          ),
        ),
        BlocProvider(
          create: (context) => ContactSaveBloc(
            repository: services.get<ContactRepository>(),
          ),
        ),
        BlocProvider(
          create: (context) => ContactBloc(
            repository: services.get<ContactRepository>(),
          ),
        ),
      ],
      child: MaterialApp.router(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.amber,
          useMaterial3: true,
          inputDecorationTheme:
              const InputDecorationTheme(border: OutlineInputBorder()),
        ),
        routerConfig: _router,
      ),
    );
  }
}
