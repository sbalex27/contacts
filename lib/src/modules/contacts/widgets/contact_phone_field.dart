import 'package:flutter/material.dart';

import '../models/contact_phone.dart';

class ContactPhoneField extends StatelessWidget {
  const ContactPhoneField({
    super.key,
    this.controller,
    this.onChanged,
    required this.phone,
  });

  final TextEditingController? controller;
  final ContactPhone phone;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        icon: const Icon(Icons.phone),
        label: const Text('Teléfono'),
        errorText: phone.pure ? null : errorText(),
      ),
      controller: controller,
      onChanged: onChanged,
    );
  }

  String? errorText() {
    return {
      ContactPhoneError.empty: 'El teléfono es requerido',
      ContactPhoneError.short: 'El teléfono es demasiado corto',
      ContactPhoneError.invalid: 'El teléfono es inválido',
      null: null
    }[phone];
  }
}
