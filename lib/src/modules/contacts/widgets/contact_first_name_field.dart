import 'package:contacts/src/modules/contacts/models/contact_first_name.dart';
import 'package:flutter/material.dart';

class ContactFirstNameField extends StatelessWidget {
  const ContactFirstNameField({
    super.key,
    this.controller,
    this.onChanged,
    required this.firstName,
  });

  final TextEditingController? controller;
  final ContactFirstName firstName;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        icon: const Icon(Icons.person),
        label: const Text('Nombres'),
        errorText: firstName.pure ? null : errorText(),
      ),
      controller: controller,
      onChanged: onChanged,
    );
  }

  String? errorText() {
    return {
      ContactFirstNameError.empty: 'El nombre es requerido',
      null: null
    }[firstName];
  }
}
