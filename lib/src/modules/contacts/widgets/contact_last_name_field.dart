import 'package:contacts/src/modules/contacts/models/contact_last_name.dart';
import 'package:flutter/material.dart';

class ContactLastNameField extends StatelessWidget {
  const ContactLastNameField({
    super.key,
    this.controller,
    this.onChanged,
    required this.lastName,
  });

  final TextEditingController? controller;
  final ContactLastName lastName;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        icon: const Icon(Icons.person_outlined),
        label: const Text('Apellidos'),
        errorText: lastName.pure ? null : errorText(),
      ),
      controller: controller,
      onChanged: onChanged,
    );
  }

  String? errorText() {
    return {
      ContactLastNameError.empty: 'El apellido es requerido',
      null: null
    }[lastName];
  }
}
