part of 'contact_bloc.dart';

abstract class ContactEvent extends Equatable {
  const ContactEvent();

  @override
  List<Object> get props => [];
}

class ContactStarted extends ContactEvent {
  const ContactStarted(this.id);

  final int id;

  @override
  List<Object> get props => [id];
}

class ContactDeletePressed extends ContactEvent {
  const ContactDeletePressed(this.id);

  final int id;

  @override
  List<Object> get props => [id];
}
