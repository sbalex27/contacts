import 'package:bloc/bloc.dart';
import 'package:contacts/src/modules/contacts/models/contact.dart';
import 'package:contacts/src/modules/contacts/models/contact_first_name.dart';
import 'package:contacts/src/modules/contacts/models/contact_phone.dart';
import 'package:contacts/src/modules/contacts/repositories/contact_repository.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import '../models/contact_last_name.dart';

part 'contact_save_bloc.g.dart';
part 'contact_save_event.dart';
part 'contact_save_state.dart';

class ContactSaveBloc extends Bloc<ContactSaveEvent, ContactSaveState> {
  ContactSaveBloc({
    required this.repository,
  }) : super(const ContactSaveState()) {
    on<ContactSaveStarted>(_onContactSaveStarted);
    on<ContactSaveFirstNameChanged>(_onContactSaveFirstNameChanged);
    on<ContactSaveLastNameChanged>(_onContactSaveLastNameChanged);
    on<ContactSavePhoneChanged>(_onContactSavePhoneChanged);
    on<ContactSaveDonePressed>(_onContactSaveDonePressed);
  }

  final ContactRepository repository;

  void _onContactSaveStarted(
    ContactSaveStarted event,
    Emitter<ContactSaveState> emit,
  ) {
    emit(const ContactSaveState());
  }

  void _onContactSaveFirstNameChanged(
    ContactSaveFirstNameChanged event,
    Emitter<ContactSaveState> emit,
  ) {
    emit(state.copyWith.firstName(ContactFirstName.dirty(event.value)));
  }

  void _onContactSaveLastNameChanged(
    ContactSaveLastNameChanged event,
    Emitter<ContactSaveState> emit,
  ) {
    emit(state.copyWith.lastName(ContactLastName.dirty(event.value)));
  }

  void _onContactSavePhoneChanged(
    ContactSavePhoneChanged event,
    Emitter<ContactSaveState> emit,
  ) {
    emit(state.copyWith.phone(ContactPhone.dirty(event.value)));
  }

  Future<void> _onContactSaveDonePressed(
    ContactSaveDonePressed event,
    Emitter<ContactSaveState> emit,
  ) async {
    final firstName = state.firstName;
    final lastName = state.lastName;
    final phone = state.phone;

    final status = Formz.validate([firstName, lastName, phone]);

    emit(state.copyWith.status(status));

    if (status.isValid) {
      final contact = Contact(
        firstName: firstName.value,
        lastName: lastName.value,
        phone: phone.value,
      );

      emit(state.copyWith.status(FormzStatus.submissionInProgress));

      try {
        final created = await repository.store(contact);

        emit(state.copyWith(
          created: created,
          status: FormzStatus.submissionSuccess,
        ));
      } catch (e) {
        emit(state.copyWith.status(FormzStatus.submissionFailure));
      }
    }
  }
}
