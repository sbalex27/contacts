part of 'contact_save_bloc.dart';

abstract class ContactSaveEvent extends Equatable {
  const ContactSaveEvent();

  @override
  List<Object> get props => [];
}

class ContactSaveFirstNameChanged extends ContactSaveEvent {
  const ContactSaveFirstNameChanged(this.value);

  final String value;

  @override
  List<Object> get props => [value];
}

class ContactSaveLastNameChanged extends ContactSaveEvent {
  const ContactSaveLastNameChanged(this.value);

  final String value;

  @override
  List<Object> get props => [value];
}

class ContactSavePhoneChanged extends ContactSaveEvent {
  const ContactSavePhoneChanged(this.value);

  final String value;

  @override
  List<Object> get props => [value];
}

class ContactSaveDonePressed extends ContactSaveEvent {
  const ContactSaveDonePressed();

  @override
  List<Object> get props => [];
}

class ContactSaveStarted extends ContactSaveEvent {
  const ContactSaveStarted();

  @override
  List<Object> get props => [];
}
