import 'package:bloc/bloc.dart';
import 'package:contacts/src/modules/contacts/models/contact.dart';
import 'package:contacts/src/modules/contacts/models/contact_first_name.dart';
import 'package:contacts/src/modules/contacts/models/contact_last_name.dart';
import 'package:contacts/src/modules/contacts/models/contact_phone.dart';
import 'package:contacts/src/modules/contacts/repositories/contact_repository.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

part 'contact_edit_bloc.g.dart';
part 'contact_edit_event.dart';
part 'contact_edit_state.dart';

class ContactEditBloc extends Bloc<ContactEditEvent, ContactEditState> {
  ContactEditBloc({
    required this.repository,
    required Contact contact,
  }) : super(ContactEditState(
            contact: contact,
            firstName: ContactFirstName.dirty(contact.firstName),
            lastName: ContactLastName.dirty(contact.lastName),
            phone: ContactPhone.dirty(contact.phone))) {
    on<ContactEditFirstNameChanged>(_onContactEditFirstNameChanged);
    on<ContactEditLastNameChanged>(_onContactEditLastNameChanged);
    on<ContactEditPhoneChanged>(_onContactEditPhoneChanged);
    on<ContactEditDonePressed>(_onContactEditDonePressed);
  }

  final ContactRepository repository;

  void _onContactEditFirstNameChanged(
    ContactEditFirstNameChanged event,
    Emitter<ContactEditState> emit,
  ) {
    emit(state.copyWith.firstName(ContactFirstName.dirty(event.value)));
  }

  void _onContactEditLastNameChanged(
    ContactEditLastNameChanged event,
    Emitter<ContactEditState> emit,
  ) {
    emit(state.copyWith.lastName(ContactLastName.dirty(event.value)));
  }

  void _onContactEditPhoneChanged(
    ContactEditPhoneChanged event,
    Emitter<ContactEditState> emit,
  ) {
    emit(state.copyWith.phone(ContactPhone.dirty(event.value)));
  }

  Future<void> _onContactEditDonePressed(
    ContactEditDonePressed event,
    Emitter<ContactEditState> emit,
  ) async {
    final firstName = ContactFirstName.dirty(state.firstName.value);
    final lastName = ContactLastName.dirty(state.lastName.value);
    final phone = ContactPhone.dirty(state.phone.value);

    final status = Formz.validate([firstName, lastName, phone]);
    emit(state.copyWith.status(status));

    if (status.isValid) {
      emit(state.copyWith.status(FormzStatus.submissionInProgress));

      final contact = Contact(
        firstName: firstName.value,
        lastName: lastName.value,
        phone: phone.value,
      );

      try {
        final edited = await repository.update(event.id, contact);

        emit(state.copyWith(
          contact: edited,
          status: FormzStatus.submissionSuccess,
        ));
      } catch (e) {
        emit(state.copyWith.status(FormzStatus.submissionFailure));
      }
    }
  }
}
