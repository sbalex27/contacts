part of 'contact_bloc.dart';

enum ContactStatus { initial, loading, success, error, deleted }

@CopyWith()
class ContactState extends Equatable {
  const ContactState({
    this.status = ContactStatus.initial,
    this.contact,
  });

  final ContactStatus status;
  final Contact? contact;

  @override
  List<Object?> get props => [status, contact];
}
