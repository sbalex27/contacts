part of 'contact_save_bloc.dart';

@CopyWith()
class ContactSaveState extends Equatable {
  const ContactSaveState({
    this.status = FormzStatus.pure,
    this.firstName = const ContactFirstName.pure(),
    this.lastName = const ContactLastName.pure(),
    this.phone = const ContactPhone.pure(),
    this.created,
  });

  final FormzStatus status;
  final ContactFirstName firstName;
  final ContactLastName lastName;
  final ContactPhone phone;
  final Contact? created;

  @override
  List<Object?> get props => [status, firstName, lastName, phone, created];
}
