part of 'contacts_bloc.dart';

enum Status { initial, loading, success, error }

@CopyWith()
class ContactsState extends Equatable {
  const ContactsState({
    this.status = Status.initial,
    this.contacts = const [],
  });

  final Status status;
  final List<Contact> contacts;

  @override
  List<Object> get props => [status, contacts];
}
