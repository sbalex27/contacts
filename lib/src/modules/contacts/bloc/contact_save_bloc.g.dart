// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_save_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$ContactSaveStateCWProxy {
  ContactSaveState created(Contact? created);

  ContactSaveState firstName(ContactFirstName firstName);

  ContactSaveState lastName(ContactLastName lastName);

  ContactSaveState phone(ContactPhone phone);

  ContactSaveState status(FormzStatus status);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactSaveState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactSaveState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactSaveState call({
    Contact? created,
    ContactFirstName? firstName,
    ContactLastName? lastName,
    ContactPhone? phone,
    FormzStatus? status,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfContactSaveState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfContactSaveState.copyWith.fieldName(...)`
class _$ContactSaveStateCWProxyImpl implements _$ContactSaveStateCWProxy {
  final ContactSaveState _value;

  const _$ContactSaveStateCWProxyImpl(this._value);

  @override
  ContactSaveState created(Contact? created) => this(created: created);

  @override
  ContactSaveState firstName(ContactFirstName firstName) =>
      this(firstName: firstName);

  @override
  ContactSaveState lastName(ContactLastName lastName) =>
      this(lastName: lastName);

  @override
  ContactSaveState phone(ContactPhone phone) => this(phone: phone);

  @override
  ContactSaveState status(FormzStatus status) => this(status: status);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactSaveState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactSaveState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactSaveState call({
    Object? created = const $CopyWithPlaceholder(),
    Object? firstName = const $CopyWithPlaceholder(),
    Object? lastName = const $CopyWithPlaceholder(),
    Object? phone = const $CopyWithPlaceholder(),
    Object? status = const $CopyWithPlaceholder(),
  }) {
    return ContactSaveState(
      created: created == const $CopyWithPlaceholder()
          ? _value.created
          // ignore: cast_nullable_to_non_nullable
          : created as Contact?,
      firstName: firstName == const $CopyWithPlaceholder() || firstName == null
          ? _value.firstName
          // ignore: cast_nullable_to_non_nullable
          : firstName as ContactFirstName,
      lastName: lastName == const $CopyWithPlaceholder() || lastName == null
          ? _value.lastName
          // ignore: cast_nullable_to_non_nullable
          : lastName as ContactLastName,
      phone: phone == const $CopyWithPlaceholder() || phone == null
          ? _value.phone
          // ignore: cast_nullable_to_non_nullable
          : phone as ContactPhone,
      status: status == const $CopyWithPlaceholder() || status == null
          ? _value.status
          // ignore: cast_nullable_to_non_nullable
          : status as FormzStatus,
    );
  }
}

extension $ContactSaveStateCopyWith on ContactSaveState {
  /// Returns a callable class that can be used as follows: `instanceOfContactSaveState.copyWith(...)` or like so:`instanceOfContactSaveState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$ContactSaveStateCWProxy get copyWith => _$ContactSaveStateCWProxyImpl(this);
}
