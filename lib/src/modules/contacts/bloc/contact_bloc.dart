import 'package:bloc/bloc.dart';
import 'package:contacts/src/modules/contacts/models/contact.dart';
import 'package:contacts/src/modules/contacts/repositories/contact_repository.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';

part 'contact_bloc.g.dart';
part 'contact_event.dart';
part 'contact_state.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  ContactBloc({
    required this.repository,
  }) : super(const ContactState()) {
    on<ContactStarted>(_onContactStarted);
    on<ContactDeletePressed>(_onContactDeletePressed);
  }

  final ContactRepository repository;

  Future<void> _onContactStarted(
    ContactStarted event,
    Emitter<ContactState> emit,
  ) async {
    emit(state.copyWith.status(ContactStatus.loading));
    try {
      final contact = await repository.show(event.id);

      emit(state.copyWith(
        status: ContactStatus.success,
        contact: contact,
      ));
    } catch (e) {
      emit(state.copyWith.status(ContactStatus.error));
    }
  }

  Future<void> _onContactDeletePressed(
    ContactDeletePressed event,
    Emitter<ContactState> emit,
  ) async {
    try {
      await repository.delete(event.id);
      emit(state.copyWith.status(ContactStatus.deleted));
    } catch (e) {
      emit(state.copyWith.status(ContactStatus.error));
    }
  }
}
