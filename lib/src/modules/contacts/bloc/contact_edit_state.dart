part of 'contact_edit_bloc.dart';

@CopyWith()
class ContactEditState extends Equatable {
  const ContactEditState({
    this.status = FormzStatus.pure,
    this.firstName = const ContactFirstName.pure(),
    this.lastName = const ContactLastName.pure(),
    this.phone = const ContactPhone.pure(),
    required this.contact,
  });

  final FormzStatus status;
  final ContactFirstName firstName;
  final ContactLastName lastName;
  final ContactPhone phone;
  final Contact contact;

  @override
  List<Object> get props => [status, firstName, lastName, phone, contact];
}
