// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contacts_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$ContactsStateCWProxy {
  ContactsState contacts(List<Contact> contacts);

  ContactsState status(Status status);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactsState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactsState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactsState call({
    List<Contact>? contacts,
    Status? status,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfContactsState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfContactsState.copyWith.fieldName(...)`
class _$ContactsStateCWProxyImpl implements _$ContactsStateCWProxy {
  final ContactsState _value;

  const _$ContactsStateCWProxyImpl(this._value);

  @override
  ContactsState contacts(List<Contact> contacts) => this(contacts: contacts);

  @override
  ContactsState status(Status status) => this(status: status);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactsState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactsState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactsState call({
    Object? contacts = const $CopyWithPlaceholder(),
    Object? status = const $CopyWithPlaceholder(),
  }) {
    return ContactsState(
      contacts: contacts == const $CopyWithPlaceholder() || contacts == null
          ? _value.contacts
          // ignore: cast_nullable_to_non_nullable
          : contacts as List<Contact>,
      status: status == const $CopyWithPlaceholder() || status == null
          ? _value.status
          // ignore: cast_nullable_to_non_nullable
          : status as Status,
    );
  }
}

extension $ContactsStateCopyWith on ContactsState {
  /// Returns a callable class that can be used as follows: `instanceOfContactsState.copyWith(...)` or like so:`instanceOfContactsState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$ContactsStateCWProxy get copyWith => _$ContactsStateCWProxyImpl(this);
}
