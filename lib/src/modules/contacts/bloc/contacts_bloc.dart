import 'package:bloc/bloc.dart';
import 'package:contacts/src/modules/contacts/models/contact.dart';
import 'package:contacts/src/modules/contacts/repositories/contact_repository.dart';
import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';

part 'contacts_bloc.g.dart';
part 'contacts_event.dart';
part 'contacts_state.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  ContactsBloc({
    required this.repository,
  }) : super(const ContactsState()) {
    on<ContactsStarted>(_onContactsStarted);
  }

  final ContactRepository repository;

  Future<void> _onContactsStarted(
    ContactsStarted event,
    Emitter<ContactsState> emit,
  ) async {
    try {
      emit(state.copyWith.status(Status.loading));

      final contacts = await repository.index();

      emit(state.copyWith(
        contacts: contacts,
        status: Status.success,
      ));
    } catch (e) {
      emit(state.copyWith.status(Status.error));
    }
  }
}
