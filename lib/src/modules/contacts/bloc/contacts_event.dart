part of 'contacts_bloc.dart';

abstract class ContactsEvent extends Equatable {
  const ContactsEvent();

  @override
  List<Object> get props => [];
}

class ContactsStarted extends ContactsEvent {
  const ContactsStarted();

  @override
  List<Object> get props => [];
}
