// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$ContactStateCWProxy {
  ContactState contact(Contact? contact);

  ContactState status(ContactStatus status);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactState call({
    Contact? contact,
    ContactStatus? status,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfContactState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfContactState.copyWith.fieldName(...)`
class _$ContactStateCWProxyImpl implements _$ContactStateCWProxy {
  final ContactState _value;

  const _$ContactStateCWProxyImpl(this._value);

  @override
  ContactState contact(Contact? contact) => this(contact: contact);

  @override
  ContactState status(ContactStatus status) => this(status: status);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactState call({
    Object? contact = const $CopyWithPlaceholder(),
    Object? status = const $CopyWithPlaceholder(),
  }) {
    return ContactState(
      contact: contact == const $CopyWithPlaceholder()
          ? _value.contact
          // ignore: cast_nullable_to_non_nullable
          : contact as Contact?,
      status: status == const $CopyWithPlaceholder() || status == null
          ? _value.status
          // ignore: cast_nullable_to_non_nullable
          : status as ContactStatus,
    );
  }
}

extension $ContactStateCopyWith on ContactState {
  /// Returns a callable class that can be used as follows: `instanceOfContactState.copyWith(...)` or like so:`instanceOfContactState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$ContactStateCWProxy get copyWith => _$ContactStateCWProxyImpl(this);
}
