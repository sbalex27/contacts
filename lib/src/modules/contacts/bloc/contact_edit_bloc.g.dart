// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_edit_bloc.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$ContactEditStateCWProxy {
  ContactEditState contact(Contact contact);

  ContactEditState firstName(ContactFirstName firstName);

  ContactEditState lastName(ContactLastName lastName);

  ContactEditState phone(ContactPhone phone);

  ContactEditState status(FormzStatus status);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactEditState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactEditState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactEditState call({
    Contact? contact,
    ContactFirstName? firstName,
    ContactLastName? lastName,
    ContactPhone? phone,
    FormzStatus? status,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfContactEditState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfContactEditState.copyWith.fieldName(...)`
class _$ContactEditStateCWProxyImpl implements _$ContactEditStateCWProxy {
  final ContactEditState _value;

  const _$ContactEditStateCWProxyImpl(this._value);

  @override
  ContactEditState contact(Contact contact) => this(contact: contact);

  @override
  ContactEditState firstName(ContactFirstName firstName) =>
      this(firstName: firstName);

  @override
  ContactEditState lastName(ContactLastName lastName) =>
      this(lastName: lastName);

  @override
  ContactEditState phone(ContactPhone phone) => this(phone: phone);

  @override
  ContactEditState status(FormzStatus status) => this(status: status);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ContactEditState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ContactEditState(...).copyWith(id: 12, name: "My name")
  /// ````
  ContactEditState call({
    Object? contact = const $CopyWithPlaceholder(),
    Object? firstName = const $CopyWithPlaceholder(),
    Object? lastName = const $CopyWithPlaceholder(),
    Object? phone = const $CopyWithPlaceholder(),
    Object? status = const $CopyWithPlaceholder(),
  }) {
    return ContactEditState(
      contact: contact == const $CopyWithPlaceholder() || contact == null
          ? _value.contact
          // ignore: cast_nullable_to_non_nullable
          : contact as Contact,
      firstName: firstName == const $CopyWithPlaceholder() || firstName == null
          ? _value.firstName
          // ignore: cast_nullable_to_non_nullable
          : firstName as ContactFirstName,
      lastName: lastName == const $CopyWithPlaceholder() || lastName == null
          ? _value.lastName
          // ignore: cast_nullable_to_non_nullable
          : lastName as ContactLastName,
      phone: phone == const $CopyWithPlaceholder() || phone == null
          ? _value.phone
          // ignore: cast_nullable_to_non_nullable
          : phone as ContactPhone,
      status: status == const $CopyWithPlaceholder() || status == null
          ? _value.status
          // ignore: cast_nullable_to_non_nullable
          : status as FormzStatus,
    );
  }
}

extension $ContactEditStateCopyWith on ContactEditState {
  /// Returns a callable class that can be used as follows: `instanceOfContactEditState.copyWith(...)` or like so:`instanceOfContactEditState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$ContactEditStateCWProxy get copyWith => _$ContactEditStateCWProxyImpl(this);
}
