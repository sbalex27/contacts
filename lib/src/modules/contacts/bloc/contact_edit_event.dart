part of 'contact_edit_bloc.dart';

abstract class ContactEditEvent extends Equatable {
  const ContactEditEvent();

  @override
  List<Object> get props => [];
}

class ContactEditFirstNameChanged extends ContactEditEvent {
  const ContactEditFirstNameChanged(this.value);

  final String value;

  @override
  List<Object> get props => [value];
}

class ContactEditLastNameChanged extends ContactEditEvent {
  const ContactEditLastNameChanged(this.value);

  final String value;

  @override
  List<Object> get props => [value];
}

class ContactEditPhoneChanged extends ContactEditEvent {
  const ContactEditPhoneChanged(this.value);

  final String value;

  @override
  List<Object> get props => [value];
}

class ContactEditDonePressed extends ContactEditEvent {
  const ContactEditDonePressed(this.id);

  final int id;

  @override
  List<Object> get props => [id];
}
