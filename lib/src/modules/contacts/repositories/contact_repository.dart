import 'package:contacts/src/modules/contacts/models/contact.dart';

abstract class ContactRepository {
  Future<List<Contact>> index();
  Future<Contact> show(int id);
  Future<Contact> store(Contact contact);
  Future<Contact> update(int id, Contact contact);
  Future<void> delete(int id);
}
