import 'package:contacts/src/modules/contacts/models/contact.dart';
import 'package:contacts/src/modules/contacts/repositories/contact_repository.dart';
import 'package:contacts/src/modules/contacts/services/contact_sqlite_data_provider.dart';
import 'package:injectable/injectable.dart';

@Singleton(as: ContactRepository)
class ContactSQLiteRepository implements ContactRepository {
  final ContactSQLiteDataProvider provider;

  const ContactSQLiteRepository({
    required this.provider,
  });

  @override
  Future<void> delete(int id) {
    return provider.delete(id);
  }

  @override
  Future<List<Contact>> index() {
    return provider
        .index()
        .then((value) => value.map((e) => Contact.fromJson(e)).toList());
  }

  @override
  Future<Contact> show(int id) {
    return provider.show(id).then((value) => Contact.fromJson(value));
  }

  @override
  Future<Contact> store(Contact contact) async {
    final id = await provider.store(contact.toJson());
    contact.copyWith.id(id);
    return contact;
  }

  @override
  Future<Contact> update(int id, Contact contact) async {
    await provider.update(id, contact.toJson());
    return contact;
  }
}
