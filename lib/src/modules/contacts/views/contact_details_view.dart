import 'package:contacts/src/modules/contacts/bloc/contact_bloc.dart';
import 'package:contacts/src/modules/contacts/bloc/contacts_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class ContactDetailsView extends StatefulWidget {
  const ContactDetailsView({super.key, required this.id});

  final int id;

  @override
  State<ContactDetailsView> createState() => _ContactDetailsViewState();
}

class _ContactDetailsViewState extends State<ContactDetailsView> {
  @override
  void initState() {
    super.initState();
    context.read<ContactBloc>().add(ContactStarted(widget.id));
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ContactBloc, ContactState>(
      listener: (context, state) {
        if (state.status == ContactStatus.deleted) {
          context.read<ContactsBloc>().add(const ContactsStarted());
          context.pop();
        }

        if (state.status == ContactStatus.error) {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text('Error'),
                content: const Text('Ha ocurrido un error inesperado'),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: const Text('Aceptar'),
                  )
                ],
              );
            },
          );
        }
      },
      builder: (context, state) {
        if (state.status == ContactStatus.loading) {
          return const Material(
            child: Center(child: CircularProgressIndicator()),
          );
        }

        return Scaffold(
          appBar: AppBar(
            title: Text('Detalles ${widget.id}'),
            actions: [
              IconButton(
                onPressed: () {
                  context
                      .read<ContactBloc>()
                      .add(ContactDeletePressed(widget.id));
                },
                icon: const Icon(Icons.delete),
              )
            ],
          ),
          body: Builder(builder: (context) {
            if ([
              ContactStatus.initial,
              ContactStatus.loading,
            ].contains(state.status)) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            return Column(
              children: [
                ListTile(
                  leading: const Icon(Icons.person),
                  title: const Text('Nombres'),
                  subtitle: Text(state.contact!.firstName),
                ),
                ListTile(
                  leading: const Icon(Icons.person_outlined),
                  title: const Text('Apellidos'),
                  subtitle: Text(state.contact!.lastName),
                ),
                ListTile(
                  leading: const Icon(Icons.phone),
                  title: const Text('Teléfono'),
                  subtitle: Text(state.contact!.phone),
                )
              ],
            );
          }),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              context.push('/details/${widget.id}/edit', extra: state.contact!);
            },
            child: const Icon(Icons.edit),
          ),
        );
      },
    );
  }
}
