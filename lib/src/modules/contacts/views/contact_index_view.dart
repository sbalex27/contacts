import 'package:contacts/src/modules/contacts/bloc/contacts_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class ContactIndexView extends StatefulWidget {
  const ContactIndexView({super.key});

  @override
  State<ContactIndexView> createState() => _ContactIndexViewState();
}

class _ContactIndexViewState extends State<ContactIndexView> {
  @override
  void initState() {
    super.initState();
    context.read<ContactsBloc>().add(const ContactsStarted());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<ContactsBloc, ContactsState>(
        builder: (context, state) {
          return CustomScrollView(
            slivers: [
              SliverAppBar.large(
                title: const Text('Contactos'),
              ),
              if (state.status == Status.loading)
                const SliverFillRemaining(
                  child: Center(child: CircularProgressIndicator()),
                ),
              if (state.status == Status.success)
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    childCount: state.contacts.length,
                    (context, index) {
                      final contact = state.contacts[index];
                      final StringBuffer letters = StringBuffer();
                      letters.write(contact.firstName.characters.first);
                      letters.write(contact.lastName.characters.first);
                      return ListTile(
                        leading: CircleAvatar(
                          child: Text(letters.toString()),
                        ),
                        title: Text(contact.firstName),
                        subtitle: Text(contact.lastName),
                        onTap: () {
                          context.push('/details/${contact.id}');
                        },
                      );
                    },
                  ),
                )
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            context.push('/create');
          },
          label: const Text('Nuevo'),
          icon: const Icon(Icons.add)),
    );
  }
}
