import 'package:contacts/src/modules/contacts/bloc/contact_bloc.dart';
import 'package:contacts/src/modules/contacts/bloc/contact_edit_bloc.dart';
import 'package:contacts/src/modules/contacts/widgets/contact_first_name_field.dart';
import 'package:contacts/src/modules/contacts/widgets/contact_last_name_field.dart';
import 'package:contacts/src/modules/contacts/widgets/contact_phone_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:go_router/go_router.dart';

class ContactEditView extends StatefulWidget {
  const ContactEditView({super.key, required this.id});

  final int id;

  @override
  State<ContactEditView> createState() => _ContactEditViewState();
}

class _ContactEditViewState extends State<ContactEditView> {
  late final TextEditingController firstNameController;
  late final TextEditingController lastNameController;
  late final TextEditingController phoneController;

  @override
  void initState() {
    super.initState();
    final initial = context.read<ContactEditBloc>().state.contact;
    firstNameController = TextEditingController(text: initial.firstName);
    lastNameController = TextEditingController(text: initial.lastName);
    phoneController = TextEditingController(text: initial.phone);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ContactEditBloc, ContactEditState>(
      listener: (context, state) {
        if (state.status.isSubmissionSuccess) {
          context.read<ContactBloc>().add(ContactStarted(widget.id));
          context.pop();
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Edit'),
            actions: [
              IconButton(
                onPressed: () {
                  context
                      .read<ContactEditBloc>()
                      .add(ContactEditDonePressed(widget.id));
                },
                icon: const Icon(Icons.done),
              )
            ],
          ),
          body: Column(
            children: [
              if (state.status.isSubmissionInProgress)
                const LinearProgressIndicator(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    ContactFirstNameField(
                      firstName: state.firstName,
                      controller: firstNameController,
                      onChanged: (value) => context
                          .read<ContactEditBloc>()
                          .add(ContactEditFirstNameChanged(value)),
                    ),
                    const SizedBox(height: 12),
                    ContactLastNameField(
                      lastName: state.lastName,
                      controller: lastNameController,
                      onChanged: (value) => context
                          .read<ContactEditBloc>()
                          .add(ContactEditLastNameChanged(value)),
                    ),
                    const SizedBox(height: 12),
                    ContactPhoneField(
                      phone: state.phone,
                      controller: phoneController,
                      onChanged: (value) => context
                          .read<ContactEditBloc>()
                          .add(ContactEditPhoneChanged(value)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
