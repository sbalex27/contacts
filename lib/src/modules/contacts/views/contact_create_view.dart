import 'package:contacts/src/modules/contacts/bloc/contact_save_bloc.dart';
import 'package:contacts/src/modules/contacts/bloc/contacts_bloc.dart';
import 'package:contacts/src/modules/contacts/widgets/contact_first_name_field.dart';
import 'package:contacts/src/modules/contacts/widgets/contact_last_name_field.dart';
import 'package:contacts/src/modules/contacts/widgets/contact_phone_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:go_router/go_router.dart';

class ContactCreateView extends StatefulWidget {
  const ContactCreateView({super.key});

  @override
  State<ContactCreateView> createState() => _ContactCreateViewState();
}

class _ContactCreateViewState extends State<ContactCreateView> {
  @override
  void initState() {
    super.initState();
    context.read<ContactSaveBloc>().add(const ContactSaveStarted());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Nuevo Contacto'),
      ),
      body: BlocConsumer<ContactSaveBloc, ContactSaveState>(
        listener: (context, state) {
          if (state.status.isSubmissionSuccess) {
            context.read<ContactsBloc>().add(const ContactsStarted());
            context.pop();
          }
        },
        builder: (context, state) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    ContactFirstNameField(
                      firstName: state.firstName,
                      onChanged: (value) => context
                          .read<ContactSaveBloc>()
                          .add(ContactSaveFirstNameChanged(value)),
                    ),
                    const SizedBox(height: 12),
                    ContactLastNameField(
                      lastName: state.lastName,
                      onChanged: (value) => context
                          .read<ContactSaveBloc>()
                          .add(ContactSaveLastNameChanged(value)),
                    ),
                    const SizedBox(height: 12),
                    ContactPhoneField(
                      phone: state.phone,
                      onChanged: (value) => context
                          .read<ContactSaveBloc>()
                          .add(ContactSavePhoneChanged(value)),
                    ),
                  ],
                ),
              ),
              ButtonBar(
                children: [
                  ElevatedButton(
                    onPressed: () {
                      context
                          .read<ContactSaveBloc>()
                          .add(const ContactSaveDonePressed());
                    },
                    child: const Text('Guardar'),
                  ),
                ],
              )
            ],
          );
        },
      ),
    );
  }
}
