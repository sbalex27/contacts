import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

@injectable
class ContactSQLiteDataProvider {
  final Database database;
  final String table = 'Contacts';

  const ContactSQLiteDataProvider({
    required this.database,
  });

  Future<List<Map<String, Object?>>> index() {
    return database.query(table);
  }

  Future<Map<String, Object?>> show(int id) {
    return database
        .query(table, where: 'id = ?', whereArgs: [id], limit: 1)
        .then((value) => value.first);
  }

  Future<int> store(Map<String, Object?> values) {
    return database.insert(table, values);
  }

  Future<int> update(int id, Map<String, Object?> values) {
    return database.update(table, values, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> delete(int id) {
    return database.delete(table, where: 'id = ?', whereArgs: [id]);
  }
}
