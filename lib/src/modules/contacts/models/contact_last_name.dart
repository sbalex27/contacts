import 'package:formz/formz.dart';

enum ContactLastNameError { empty }

class ContactLastName extends FormzInput<String, ContactLastNameError> {
  const ContactLastName.pure() : super.pure('');
  const ContactLastName.dirty(String value) : super.dirty(value);

  @override
  ContactLastNameError? validator(String value) {
    if (value.isEmpty) return ContactLastNameError.empty;
    return null;
  }
}
