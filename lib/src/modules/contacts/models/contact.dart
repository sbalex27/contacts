import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contact.g.dart';

@CopyWith()
@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class Contact {
  final int? id;
  final String firstName;
  final String lastName;
  final String phone;

  const Contact({
    this.id,
    required this.firstName,
    required this.lastName,
    required this.phone,
  });

  factory Contact.fromJson(Map<String, dynamic> json) {
    return _$ContactFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$ContactToJson(this);
  }
}
