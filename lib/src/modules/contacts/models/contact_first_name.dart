import 'package:formz/formz.dart';

enum ContactFirstNameError { empty }

class ContactFirstName extends FormzInput<String, ContactFirstNameError> {
  const ContactFirstName.pure() : super.pure('');
  const ContactFirstName.dirty(String value) : super.dirty(value);

  @override
  ContactFirstNameError? validator(String value) {
    if (value.isEmpty) return ContactFirstNameError.empty;
    return null;
  }
}
