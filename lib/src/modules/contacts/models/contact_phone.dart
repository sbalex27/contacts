import 'package:formz/formz.dart';

enum ContactPhoneError { empty, invalid, short }

class ContactPhone extends FormzInput<String, ContactPhoneError> {
  const ContactPhone.pure() : super.pure('');
  const ContactPhone.dirty(String value) : super.dirty(value);

  @override
  ContactPhoneError? validator(String value) {
    if (value.isEmpty) return ContactPhoneError.empty;
    if (value.length < 8) return ContactPhoneError.short;
    if (!RegExp(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
        .hasMatch(value)) return ContactPhoneError.invalid;
    return null;
  }
}
