import 'package:contacts/src/shared/services.config.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';

final services = GetIt.instance;

@InjectableInit()
Future<void> initializeServices() async {
  $initGetIt(services);
}

@module
abstract class RegisterModule {
  @singleton
  Future<Database> get database async {
    return openDatabase('contacts.db', version: 1,
        onCreate: (db, version) async {
      await db.execute(
          'CREATE TABLE Contacts (id INTEGER PRIMARY KEY, first_name TEXT, last_name TEXT, phone TEXT)');
    });
  }
}
