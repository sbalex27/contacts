// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:sqflite/sqflite.dart' as _i3;

import '../modules/contacts/repositories/contact_repository.dart' as _i5;
import '../modules/contacts/repositories/contact_sqlite_repository.dart' as _i6;
import '../modules/contacts/services/contact_sqlite_data_provider.dart' as _i4;
import 'services.dart' as _i7; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  final registerModule = _$RegisterModule();
  gh.singletonAsync<_i3.Database>(() => registerModule.database);
  gh.factoryAsync<_i4.ContactSQLiteDataProvider>(() async =>
      _i4.ContactSQLiteDataProvider(
          database: await get.getAsync<_i3.Database>()));
  gh.singletonAsync<_i5.ContactRepository>(() async =>
      _i6.ContactSQLiteRepository(
          provider: await get.getAsync<_i4.ContactSQLiteDataProvider>()));
  return get;
}

class _$RegisterModule extends _i7.RegisterModule {}
