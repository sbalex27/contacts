# App de Contactos

Proyecto realizado en su totalidad por Sergio Alexander Batres Escobedo, una aplicación para manejar un catálogo de contactos.

> El código fuente principal está colocado dentro del directorio `/lib` por favor tomarlo en cuenta al momento de analizar el repositorio

## Instalación
Para compilar y ejecutar esta aplicación en Android se requiere la instalación del Framework Flutter y ciertos plugins para Visual Studio Code (de preferencia), para leer la documentación oficial acerca de la instalación por favor lea la [documentación oficial de instalación para Windows](https://docs.flutter.dev/get-started/install/windows).

Después de verificar que la instalación del framework se haya realizado correctamente, clone este repositorio, ingrese a su respectivo directorio y posteriormente ejecute:
```bash
flutter pub get
```
Este comando instalará todas las dependencias necesarias automáticamente en el proyecto para que este funcione y compile correctamente (incluyendo SQLite).

## Buenas Prácticas
Se utilizaron las mejores prácticas de de desarrollo de software para implementar esta solución, está programada en el lenguaje de programación Dart que compila a Kotlin y Java en Android, utiliza el Framework [Flutter](https://docs.flutter.dev/). 

La arquitectura usada en este proyecto incluye prácticas como:
1. Patrón de diseño *Repository*
2. Patrón de diseño *BLoC (Business Logic Components)*
3. Contenedor de Inyección de Dependencias *DI*
4. Uso de *Material Design Components* para construir una interfaz de usuario amigable
